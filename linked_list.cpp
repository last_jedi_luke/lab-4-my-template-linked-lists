//
// Created by Luke on 9/28/2017.
//

#include "linked_list.h"

linked_list::linked_list() {
    head = NULL;
    end = NULL;
}
linked_list::~linked_list(){

    /*for (node *temp= head->next; head!=nullptr; temp = head->next){
        delete head;
        head = temp;
    }*/

    node *temp = head;
    while (head != NULL){
        temp = temp->next;
        delete head;
        head=temp;
    }




    std::cout<< "Destructor Called" << std::endl;

}
void linked_list::makenode(int value){
    node *temp = new node;
    temp->data = value;
    temp->next = NULL;
    if (head == NULL){      //check if it's an empty linked list
        head = temp;
        end = temp;
        temp = NULL;

    }
    else {
        end->next = temp;
        end = temp;
    }

}

void linked_list::print() {
    node *temp = new node;
    temp = head;
    while (temp != NULL) {
        std::cout << temp->data << std::endl;
        temp = temp->next;
    }
}

void linked_list::print_middle(){
    node *temp = head;
    int count = 1;

   // temp = head;
    while(temp != NULL){      //counts until it finds the end of the linked lists (ie the length of the list)
        temp = temp->next;
        count ++;
    }
    int middle = count%2;
    int divide = count/2;       //example: 7 elements divided by 2 = 3.
    if(middle == 1){        //if there is a remainder in the count (7%2 = 1)
        divide = divide +1;     //we want to print position 4 (1, 2, 3, , !4!, 5, 6, 7) because it is the middle
    }
    delete temp;        //deallocate memory
    node *temp2 = new node;
    temp2 = head;
    for (int i = 0; i < (divide-1); i++){
        temp2 = temp2->next;
    }

    std::cout << "Number of elements = " << count << std::endl;
    std::cout << "Middle is " << temp2->data << std::endl;
}

void linked_list::insert_head(int value){
    node *temp = new node;
    temp->data = value;
    temp->next = head;
    head = temp;
}

void linked_list::insert_pos(int pos, int value){       //use insert_end to add node to the end or simply use the make_node function
    if (pos == 1) {
        node *temp = new node;
        temp->data = value;
        temp->next = head;
        head = temp;
    } else {
        node *prev = new node;
        node *current = new node;
        node *temp = new node;
        current = head;
        for (int i = 0; i < pos; i++) {
            prev = current;
            current = current->next;
        }
        temp->data = value;
        prev->next = temp;
        temp->next = current;
    }
}

void linked_list::add_array(int values[], int length){
    int i = 0;


    if (head == NULL){      //if it is an empty linked list
        node *temp2 = new node;
        temp2->data = values[i];
        temp2->next = NULL;
        head = temp2;
        end = temp2;
        temp2 = NULL;
        i++;

    }
    for (i; i < length; i++){
        node* temp = new node;
        temp->data = values[i];
        temp->next = NULL;
        end->next = temp;
        end = temp;
    }
}

void linked_list::remove(int pos){
    if (pos == 1) {
        node *temp = new node;
        temp = head;
        head = head->next;
        delete temp;
    } else {
        node *current = new node;
        node *prev = new node;
        current = head;
        for (int i = 0; i < pos; i++) {
            prev = current;
            current = current->next;
        }
        prev->next = current->next;
    }
}

void linked_list::remove_last(){
    node *current = new node;
    node*prev = new node;
    current = head;
    while (current->next != NULL){
        prev = current;
        current = current->next;

    }
    end = prev;
    prev->next = NULL;
    delete current;
}

void linked_list::replace_data(int pos, int value){     //replace data of particular node
    node *temp2 = new node;
    temp2 = head;
    for (int i = 0; i < (pos-1); i++){
        temp2 = temp2->next;
    }
std::cout << "Value at position " << " was " << temp2->data << std::endl;
    temp2->data = value;
    std::cout << "Value is now " << temp2->data << std::endl;

}

void linked_list::get_value(int pos) {      //output value of particular node
    node *temp2 = new node;
    temp2 = head;
    for (int i = 0; i < (pos-1); i++){
        temp2 = temp2->next;
    }


    std::cout << "Position " << pos << " holds the value of " << temp2->data << std::endl;

}

void linked_list::insert_end(int value){        //inserts at end,
    node *temp = new node;
    temp->data = value;
    temp->next = NULL;
    end->next = temp;
    end = temp;

}
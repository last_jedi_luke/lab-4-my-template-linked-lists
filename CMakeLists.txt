cmake_minimum_required(VERSION 3.8)
project(lab4_my_template)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp linked_list.h linked_list.cpp)
add_executable(lab4_my_template ${SOURCE_FILES})
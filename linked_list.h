//
// Created by Luke on 9/28/2017.
//

#ifndef LAB4_MY_TEMPLATE_LINKED_LIST_H
#define LAB4_MY_TEMPLATE_LINKED_LIST_H

#include "iostream"
struct node{        //creates node structure
    int data;       //my apologies for not using the template, I couldn't work with it, so I did it another way that works the same.
    node *next;
};

class linked_list{
public:
    node *head;     //points to first node
    node *end;      //points to last node
    linked_list();      //initialize head and end to node
    void makenode(int value);       //can create head and can add to end of list

    void print();
    ~linked_list();     //destructor

    void print_middle();

    void insert_head(int value);        //inserts into head
    void insert_pos (int pos, int value);   //can insert into head if pos = 1, cannot insert into the end
    void insert_end (int value);    //inserts node at the end
    void add_array(int values[], int length);       //set to handle empty arrays and handle an array of size five, same as append
    void remove (int pos);  //note that position starts at 1
    void remove_last();
    void replace_data(int pos, int value);      //replace data at given node
    void get_value(int pos);        //retrieve value from node position




};

#endif //LAB4_MY_TEMPLATE_LINKED_LIST_H
